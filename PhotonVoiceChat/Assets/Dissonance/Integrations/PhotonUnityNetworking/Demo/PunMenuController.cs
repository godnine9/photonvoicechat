﻿using System;
using Photon;
using UnityEngine;
using UnityEngine.UI;

namespace Dissonance.Integrations.PhotonUnityNetworking.Demo
{
    public class PunMenuController
        : PunBehaviour
    {
        public Text Status;
        public GameObject Lobby;
        public GameObject Room;
        public GameObject JointBTN;
        public GameObject CreateBTN;
        private string _guiCreateNamedRoomName = Guid.NewGuid().ToString().Substring(0, 10);
        private State _state;
        private enum State
        {
            JoiningLobby,
            InLobby,
            JoiningRoom,
            CreatingRoom,
            InRoom
        }

        [SerializeField] public string SceneName = "PUN Game World";

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            var objs = FindObjectsOfType<PunMenuController>();
            if (objs.Length > 1)
                Destroy(gameObject);
        }

        private void Start ()
        {
            PhotonNetwork.NetworkStatisticsEnabled = true;
            PhotonNetwork.autoJoinLobby = true;
            PhotonNetwork.ConnectUsingSettings("0.1");

            _state = State.JoiningLobby;
        }

        public override void OnJoinedLobby()
        {
            _state = State.InLobby;
            Lobby.SetActive(true);
            Room.SetActive(false);
        }

        public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
        {
            _state = State.InLobby;
        }

        public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
        {
            _state = State.InLobby;
        }

        public override void OnJoinedRoom()
        {
            PhotonNetwork.LoadLevel(SceneName ?? "PUN Game World");
            _state = State.InRoom;
            Lobby.SetActive(false);
            Room.SetActive(true);
        }

        private void Update()
        {
            switch (_state)
            {
                case State.JoiningLobby:
                    Status.text = "Connecting To Photon Servers...";
                    break;

                case State.InLobby:
                    Status.text = "";
                    break;

                case State.JoiningRoom:
                    Status.text = "Joining Room...";
                    break;

                case State.CreatingRoom:
                    Status.text = "Creating Room...";
                    break;

                case State.InRoom:
                    Status.text = "";
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
            var rooms = PhotonNetwork.GetRoomList();
            if (rooms.Length > 0)
            {
                JointBTN.SetActive(true);
                CreateBTN.SetActive(false);
            }
            else
            {
                JointBTN.SetActive(false);
                CreateBTN.SetActive(true);
            }
        }

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
            PhotonNetwork.LoadLevel("PUN Demo");
        }

        public void JoinRandomRoom()
        {
            PhotonNetwork.JoinRandomRoom();
            _state = State.JoiningRoom;
        }

        public void CreateRoom()
        {
            PhotonNetwork.CreateRoom(_guiCreateNamedRoomName);
            _state = State.CreatingRoom;
        }
    }
}
